import styled from 'styled-components';

export const Wrapper = styled.div``;

const StyledInput = styled.input`
  opacity: ${p => p.theme.form.input.opacity};
  background-color: ${p => p.theme.form.input.backgroundColor};
  border-style: solid;
  border-color: ${p => p.theme.form.input.borderColor};
  border-width: ${p => p.theme.form.input.borderWidth};
  border-radius: ${p => p.theme.form.input.borderRadius};
  box-shadow: ${p => p.theme.form.input.boxShadow};
  padding: 0.6rem 0.8rem;
  width: 100%;
  font-size: ${p => p.theme.form.input.fontSize};
  color: ${p => p.theme.color};

  /* prettier-ignore */
  ${p => (p.hasHint ? 'border-bottom-left-radius: 0;' : '')}
  ${p => (p.hasHint ? 'border-bottom-right-radius: 0;' : '')}

  &:focus {
    opacity: ${p => p.theme.form.input.focusOpacity};
    background-color: ${p => p.theme.form.input.focusBackgroundColor};
    border-color: ${p => p.theme.form.input.focusBorderColor};
    box-shadow: ${p => p.theme.form.input.focusBoxShadow};
  }

  &:disabled {
    opacity: ${p => p.theme.form.input.disabledOpacity};
  }
`;

export default StyledInput;
