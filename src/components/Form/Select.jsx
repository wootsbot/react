import React from 'react';
import VirtualizedSelect from 'react-virtualized-select';

import StyledSelect from './styles/Select.style';

class Select extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <StyledSelect
        onChange={selectValue => this.setState({ selectValue })}
        value={this.state.selectValue}
        {...this.props}
      />
    );
  }
}

Select.propTypes = {
  ...VirtualizedSelect.PropTypes
};

Select.defaultProps = {
  ...VirtualizedSelect.defaultProps
};

Select.displayName = 'Form.Select';
Select.style = StyledSelect;

export default Select;
