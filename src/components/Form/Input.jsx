import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Hint from './Hint';
import StyledInput, { Wrapper } from './styles/Input.style';

const Input = ({
  type,
  disabled,
  hint,
  className,
  innerRef,
  style,
  ...props
}) => (
  <Wrapper className="input-container" style={style}>
    <StyledInput
      className={classnames(className, 'input')}
      type={type}
      disabled={disabled}
      innerRef={innerRef}
      hasHint={!!hint}
      {...props}
    />
    {!!hint && <Hint disabled={disabled}>{hint}</Hint>}
  </Wrapper>
);

Input.propTypes = {
  type: PropTypes.string,
  disabled: PropTypes.bool,
  hint: PropTypes.string,
  innerRef: PropTypes.func,
  className: PropTypes.string,
  style: PropTypes.object
};

Input.defaultProps = {
  type: 'text',
  disabled: false,
  hint: '',
  innerRef: null,
  className: '',
  style: null
};

Input.displayName = 'Form.Input';
Input.wrapperStyle = Wrapper;
Input.style = StyledInput;

export default Input;
