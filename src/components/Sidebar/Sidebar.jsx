import React from 'react';
import PropTypes from 'prop-types';
import CSSTransition from 'react-transition-group/CSSTransition';
import classnames from 'classnames';

import Loader from 'components/Loader';
import { Title, Header, Footer, Scrollable } from 'components/Layout';

import StyledSidebar, { slideDuration } from './Sidebar.style';

class Sidebar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      opened: props.opened
    };
  }
  componentWillMount() {
    this.setState({ opened: this.props.opened });
  }

  componentDidMount() {
    if (this.props.opened) this._triggerCallback('onOpen');
    if (this.props.maximized) this._triggerCallback('onMaximize');
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.opened !== nextProps.opened)
      this.setState({ opened: nextProps.opened });
  }

  componentDidUpdate(prevProps, prevState) {
    let { maximized, opened } = this.props;

    if (maximized !== prevProps.maximized) {
      if (maximized) this._triggerCallback('onMaximize');
      else this._triggerCallback('onUnmaximize');
    }

    if (opened !== prevProps.opened) {
      if (opened) this._triggerCallback('onOpen');
      else this._triggerCallback('onClose');
      return;
    }

    maximized = this.state.maximized;
    opened = this.state.opened;

    if (opened !== prevState.opened) {
      if (opened) this._triggerCallback('onOpen');
      else this._triggerCallback('onClose');
      return;
    }
  }

  componentWillUnmount() {
    if (this.props.onClose) this.props.onClose();
  }

  _triggerCallback = name => {
    if (this.props[name]) this.props[name]();
  };

  _handleCloseClick = () => {
    this.setState({ opened: false });

    this._triggerCallback('onClickClose');
  };

  render() {
    const {
      title,
      header,
      footer,
      children,
      loading,
      loaderLabel,
      position,
      width,
      maximized,
      className,
      ...rest
    } = this.props;

    const asideClasses = classnames(className, {
      [position]: true,
      [width]: true,
      maximized
    });

    const backButton = this.props.onClickBack ? (
      <button
        className="back-btn"
        onClick={() => this._triggerCallback('onClickBack')}
      >
        <i className="fas fa-chevron-left" />
      </button>
    ) : (
      <button className="back-btn" />
    );

    const closeButton = (
      <button className="close-btn" onClick={this._handleCloseClick}>
        <i className="fas fa-times" />
      </button>
    );

    const headerContent = (
      <div className="headerContent">
        {title && <Title>{title}</Title>}
        {!loading && header && header}
      </div>
    );

    return (
      <CSSTransition
        classNames="slide"
        appear
        mountOnEnter
        unmountOnExit
        in={this.state.opened}
        timeout={slideDuration}
      >
        <StyledSidebar className={asideClasses} {...rest}>
          <Header>
            {backButton}
            {headerContent}
            {closeButton}
          </Header>

          <Scrollable className="content">
            {loading ? <Loader centered label={loaderLabel} /> : children}
          </Scrollable>

          <Footer>{!loading && footer && footer}</Footer>
        </StyledSidebar>
      </CSSTransition>
    );
  }
}

Sidebar.propTypes = {
  title: PropTypes.string,
  header: PropTypes.node,
  footer: PropTypes.node,
  loading: PropTypes.bool,
  loaderLabel: PropTypes.node,
  position: PropTypes.oneOf(['left', 'right']),
  width: PropTypes.oneOf(['xs', 'sm', 'md', 'lg']),
  opened: PropTypes.bool,
  maximized: PropTypes.bool,
  onOpen: PropTypes.func,
  onClose: PropTypes.func,
  onClickClose: PropTypes.func,
  onClickBack: PropTypes.func,
  onMaximize: PropTypes.func,
  onUnmaximize: PropTypes.func,
  className: PropTypes.string,
  children: PropTypes.node.isRequired
};

Sidebar.defaultProps = {
  title: '',
  header: '',
  footer: '',
  loading: false,
  loaderLabel: '',
  position: 'left',
  width: 'md',
  maximized: false,
  opened: true,
  onOpen: null,
  onClickClose: null,
  onClose: null,
  onClickBack: null,
  onMaximize: null,
  onUnmaximize: null,
  className: ''
};

Sidebar.displayName = 'Sidebar';
Sidebar.style = StyledSidebar;

export default Sidebar;
