import styled from 'styled-components';

export const Header = styled.header`
  z-index: 1000;
  left: 0;
  right: 0;
  display: flex;
  max-width: 100%;
  pointer-events: none;

  &.container-parent {
    position: absolute;
  }
  &.container-root {
    position: fixed;
  }

  &.position-top-center,
  &.position-top-left,
  &.position-top-right {
    top: 0;
  }

  &.position-bottom-center,
  &.position-bottom-left,
  &.position-bottom-right {
    bottom: 0;
  }

  &.position-top-center,
  &.position-bottom-center {
    justify-content: center;
  }

  &.position-top-left,
  &.position-bottom-left {
    justify-content: start;
  }

  &.position-top-right,
  &.position-bottom-right {
    justify-content: end;
  }

  &.maximized {
    top: 0;
    bottom: 0;
  }
`;

export const Footer = Header.withComponent('footer');

export const StyledTitlebar = styled.div`
  transition: all 0.1s ease-out;

  font-weight: ${p => p.theme.titlebar.fontWeight};
  padding-left: ${p => p.theme.titlebar.horizontalPadding};
  padding-right: ${p => p.theme.titlebar.horizontalPadding};
  color: ${p => p.theme.titlebar.color};
  background: ${p => p.theme.titlebar.backgroundColor};
  box-shadow: ${p => p.theme.titlebar.boxShadow};
  text-align: center;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  pointer-events: all;

  &.floating {
    display: inline-block;
    border-radius: ${p => p.theme.titlebar.borderRadius};
    margin-top: ${p => p.theme.titlebar.floatMargin};
    margin-bottom: ${p => p.theme.titlebar.floatMargin};
    margin-left: ${p => p.theme.titlebar.floatMargin};
    margin-right: ${p => p.theme.titlebar.floatMargin};
  }

  &.block {
    display: block;
    width: 100%;
  }

  &.xs {
    height: ${p => p.theme.titlebar.xsHeight};
    font-size: ${p => p.theme.titlebar.xsFontSize};
    line-height: ${p => p.theme.titlebar.xsHeight};
  }

  &.sm {
    height: ${p => p.theme.titlebar.smHeight};
    font-size: ${p => p.theme.titlebar.smFontSize};
    line-height: ${p => p.theme.titlebar.smHeight};
  }

  &.md {
    height: ${p => p.theme.titlebar.mdHeight};
    font-size: ${p => p.theme.titlebar.mdFontSize};
    line-height: ${p => p.theme.titlebar.mdHeight};
  }

  &.lg {
    height: ${p => p.theme.titlebar.lgHeight};
    font-size: ${p => p.theme.titlebar.lgFontSize};
    line-height: ${p => p.theme.titlebar.lgHeight};
  }

  &.maximized {
    display: block;
    height: auto;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
  }
`;
