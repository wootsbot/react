import React from 'react';

import StyledList from './List.style';

const List = props => <StyledList {...props} />;

List.propTypes = {};
List.defaultProps = {};

List.displayName = 'List';
List.style = StyledList;

export default List;
