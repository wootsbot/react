import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import StyledAlert, { contexts } from './Alert.style';

const Alert = ({ context, className, ...props }) => {
  const classes = classnames(className, {
    alert: true,
    [`alert-${context}`]: true
  });

  return <StyledAlert className={classes} {...props} />;
};

Alert.propTypes = {
  context: PropTypes.oneOf(contexts),
  className: PropTypes.string
};

Alert.defaultProps = {
  context: 'info',
  className: ''
};

Alert.displayName = 'Alert';
Alert.style = StyledAlert;
Alert.contexts = contexts;

export default Alert;
