import React from 'react';
import PropTypes from 'prop-types';

import Titlebar from 'components/Titlebar';
import Suggestion from './Suggestion';
import { formatOsmTags, formatDeletedTags, fixOsmTags } from 'helpers/osmose';
import { osmoseSuggestionTypes as types } from 'constants/index';

import StyledOsmose from './Osmose.style';

class Osmose extends React.PureComponent {
  constructor(props) {
    super(props);

    this.renderOptions = this.renderOptions.bind(this);
  }

  renderOptions(data) {
    if (!data.elems) return null;

    const osmData = data.elems.length > 0 ? data.elems[0] : null;

    if (osmData) {
      // const osmId = osmData ? osmData.id : null;
      // const osmType = osmData ? osmData.type : null;
      const osmTags = osmData ? osmData.tags : [];
      const fixes = osmData ? osmData.fixes : [];

      const formattedTags = formatOsmTags(osmTags);

      return (
        <div>
          <Suggestion
            type={types.ORIGINAL}
            osm={osmTags}
            handleClick={() =>
              this.props.handleSuggestion(formattedTags, formattedTags)
            }
            key={0}
          />
          {fixes.map(fix => (
            <Suggestion
              type={types.MODIFICATION}
              number={fix.num + 1}
              fixes={formatDeletedTags(fix, osmTags)}
              handleClick={() =>
                this.props.handleSuggestion(
                  fixOsmTags(osmTags, fix),
                  formattedTags
                )
              }
              key={fix.num + 1}
            />
          ))}
        </div>
      );
    }

    if (!data.new_elems) return null;

    const newData = data.new_elems.length > 0 ? data.new_elems[0] : null;

    if (!newData) return null;

    return (
      <div>
        <Suggestion
          type={types.NEW}
          fixes={newData}
          handleClick={() =>
            this.props.handleSuggestion(formatOsmTags(newData.add))
          }
          className="new"
        />
      </div>
    );
  }

  render() {
    const { data } = this.props;

    return (
      <StyledOsmose>
        {data.title && <Titlebar size="sm">{data.title}</Titlebar>}
        <div>
          {data.subtitle && <h3>{data.subtitle}</h3>}
          {this.renderOptions(data)}
        </div>
      </StyledOsmose>
    );
  }
}

Osmose.propTypes = {
  data: PropTypes.object.isRequired,
  handleSuggestion: PropTypes.func.isRequired,
  className: PropTypes.string
};

Osmose.defaultProps = {
  className: ''
};

Osmose.displayName = 'Osmose';
Osmose.style = StyledOsmose;

export default Osmose;
