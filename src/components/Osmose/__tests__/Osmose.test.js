import 'jest-styled-components';
import { snapshotWithElementChildren } from 'helpers/tests';
import Osmose from '..';

import { fixData } from 'helpers/__mocks__/osmose';

describe('When using snapshots', () => {
  it('Should render with element children ', () =>
    snapshotWithElementChildren(Osmose, {
      data: fixData,
      handleSuggestion: jest.fn()
    }));

  it('Should render with empty tag data ', () => {
    snapshotWithElementChildren(Osmose, {
      data: {},
      handleSuggestion: jest.fn()
    });

    snapshotWithElementChildren(Osmose, {
      data: {
        elems: [],
        new_elems: []
      },
      handleSuggestion: jest.fn()
    });
  });
});
