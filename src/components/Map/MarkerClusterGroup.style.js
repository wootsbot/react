import { injectGlobal } from 'styled-components';

/* eslint-disable no-unused-expressions */
injectGlobal`
  .osm-ui-react-marker-cluster {
    & > div {
      width: 40px;
      height: 40px;
      border-radius: 50%;
      font-size: 0.9rem;
      font-weight: 700;
      display: flex;
      justify-content: center;
      align-items: center;
    }
  }
`;
/* eslint-enable */
