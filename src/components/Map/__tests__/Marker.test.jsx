import React from 'react';
import 'jest-styled-components';
import { snapshotWithElementChildren } from 'helpers/tests';
import Map from '..';

const position = { position: [12.3, 45.6] };

describe('When using snapshots', () => {
  it('Should render with no children and no optional props', () =>
    snapshotWithElementChildren(Map.Marker, { ...position }));

  it('Should render with no children and a theme', () =>
    snapshotWithElementChildren(Map.Marker, { ...position, theme: 'red' }));

  it('Should render with no children and a color', () =>
    snapshotWithElementChildren(Map.Marker, {
      ...position,
      color: 'hsl(273, 80%, 80%)'
    }));

  it('Should render with no children and a shape', () =>
    snapshotWithElementChildren(Map.Marker, {
      ...position,
      shape: 'basicDiamond'
    }));

  it('Should render with no children and an icon', () =>
    snapshotWithElementChildren(Map.Marker, { ...position, icon: 'home' }));
});
