import React from 'react';
import 'jest-styled-components';
import { snapshotWithElementChildren } from 'helpers/tests';
import Map from '..';

describe('When using snapshots', () => {
  it('Should render with no children and no props', () =>
    snapshotWithElementChildren(Map.MarkerClusterGroup));

  it('Should render with no children and a theme', () =>
    snapshotWithElementChildren(Map.MarkerClusterGroup, { theme: 'red' }));

  it('Should render with no children and a color', () =>
    snapshotWithElementChildren(Map.MarkerClusterGroup, {
      color: 'hsl(273, 80%, 80%)'
    }));
});
