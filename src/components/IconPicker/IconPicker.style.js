import styled from 'styled-components';

const StyledIconPicker = styled.div`
  width: 100%;
  height: 40rem;

  .header {
    width: 100%;
    height: 3rem;
    cursor: pointer;
    line-height: 3rem;
    background-color: ${props => props.theme.backgroundColor};
    color: ${props => props.theme.color};
    text-align: center;
    border-top-left-radius: 1rem;
    border-top-right-radius: 1rem;
    border: none;
    border-bottom: 2px solid ${props => props.theme.color};
  }

  .content {
    position: relative;
    display: flex;
    height: 100%;
  }

  .categories {
    position: absolute;
    width: 4rem;

    transition: width 0.5s ease-in-out;

    &.opened {
      width: 100%;
    }

    .category {
      display: flex;
      align-items: center;
      padding: 0.3rem 0;
      font-size: 1.5rem;
      background-color: ${props => props.theme.backgroundColor};
      color: ${props => props.theme.color};
      font-size: 2rem;
      cursor: pointer;
      overflow: hidden;
      white-space: nowrap;

      i {
        flex: 0 0 4rem;
        font-size: 2.2rem;
        text-align: center;
      }

      div {
        flex: 1 0;
        padding-left: 3rem;
      }

      &.current {
        color: ${props => props.theme.backgroundColor};
        background-color: ${props => props.theme.color};
      }
    }
  }

  .icons {
    margin-left: 4rem;
    padding: 0.5rem 1rem;
    font-size: 2rem;

    i {
      cursor: pointer;
      font-size: 4rem;
      margin: 0.7rem;

      &.selected {
        color: ${props => props.theme.backgroundColor};
      }
    }
  }
`;

export default StyledIconPicker;
