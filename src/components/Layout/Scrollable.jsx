import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { isOverflowYHidden } from 'helpers/dom';

import StyledScrollable from './Scrollable.style';

class Scrollable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      displayUp: false,
      displayDown: false
    };

    this.checkScroll = this.checkScroll.bind(this);

    this.scrollable = React.createRef();
  }

  componentDidMount() {
    this.checkScroll();
  }

  componentDidUpdate(prevProps) {
    if (this.props.children.length !== prevProps.children.length)
      this.checkScroll();
  }

  checkScroll() {
    const element = this.scrollable.current;

    if (element && isOverflowYHidden(element)) {
      const hasMoreDown = // percentage calculation to avoid precision issues
        1 - (element.scrollTop + element.clientHeight) / element.scrollHeight >
        0.01;
      const hasMoreUp = element.scrollTop > 0;

      this.setState({
        displayUp: hasMoreUp,
        displayDown: hasMoreDown
      });
    }
  }

  render() {
    const { displayUp, displayDown } = this.state;
    const { className, position, children } = this.props;
    const classes = classnames(
      className,
      displayUp && 'up',
      displayDown && 'down'
    );

    return (
      <StyledScrollable className={classes} position={position}>
        <div
          className="content"
          ref={this.scrollable}
          onScroll={this.checkScroll}
        >
          {children}
        </div>
      </StyledScrollable>
    );
  }
}

Scrollable.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  position: PropTypes.oneOf(['right', 'left'])
};

Scrollable.defaultProps = {
  className: '',
  position: 'left'
};

Scrollable.displayName = 'Scrollable';
Scrollable.style = StyledScrollable;

export default Scrollable;
