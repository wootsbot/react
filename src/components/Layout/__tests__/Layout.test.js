import 'jest-styled-components';
import { snapshotWithElementChildren } from 'helpers/tests';
import { Title, Header, Footer, Nav, Scrollable } from '../';

jest.mock('helpers/dom');

[Title, Header, Footer, Nav, Scrollable].forEach(component => {
  describe(`When using snapshots, ${component.displayName}`, () => {
    it('should render with an element children', () =>
      snapshotWithElementChildren(component));

    it('should render with the className prop', () =>
      snapshotWithElementChildren(component, { className: 'my-class' }));

    it('should render with other props', () =>
      snapshotWithElementChildren(component, { oneProp: 1, twoProp: 2 }));

    it('should render with className and other props', () =>
      snapshotWithElementChildren(component, {
        className: 'my-class',
        oneProp: 1,
        twoProp: 2
      }));
  });
});
