import React from 'react';
import PropTypes from 'prop-types';
import Transition from 'react-transition-group/Transition';
import classnames from 'classnames';

import { Title } from 'components/Layout';
import Loader from 'components/Loader';

import StyledModal, { Content } from './Modal.style';

class Modal extends React.Component {
  componentDidMount() {
    this._triggerCallback('onOpen');
  }

  componentWillUnmount() {
    this._triggerCallback('onClose');
  }

  _triggerCallback(name) {
    if (this.props[name] !== null) {
      this.props[name]();
    }
  }

  render() {
    const {
      container,
      scrollContent,
      className,
      title,
      loading,
      loaderLabel,
      header,
      footer,
      children,
      ...rest
    } = this.props;

    const asideClasses = classnames(className, {
      [`container-${container}`]: true,
      'scroll-content': scrollContent
    });

    const contentClasses = classnames({
      content: true,
      loading
    });

    const transitionStyles = {
      entered: {
        opacity: 1
      }
    };

    return (
      <Transition
        in
        timeout={300}
        appear
        onEntered={this.onOpen}
        onExited={this.onClose}
      >
        {state => (
          <StyledModal>
            <Content
              className={asideClasses}
              style={transitionStyles[state]}
              {...rest}
            >
              {!loading &&
                (header || title) && (
                  <header className="header">
                    {title && <Title inHeader>{title}</Title>}
                    {!loading && header && header}
                  </header>
                )}

              <div className={contentClasses}>{children}</div>

              {!loading && footer && footer}

              {loading && <Loader centered label={loaderLabel} />}
            </Content>
          </StyledModal>
        )}
      </Transition>
    );
  }
}

Modal.propTypes = {
  title: PropTypes.string,
  header: PropTypes.node,
  footer: PropTypes.node,
  loading: PropTypes.bool,
  loaderLabel: PropTypes.node,
  container: PropTypes.oneOf(['parent', 'root']),
  scrollContent: PropTypes.bool,
  onOpen: PropTypes.func,
  onClose: PropTypes.func,
  className: PropTypes.string,
  children: PropTypes.node
};

Modal.defaultProps = {
  title: '',
  header: '',
  footer: '',
  loading: false,
  loaderLabel: '',
  container: 'root',
  scrollContent: false,
  onOpen: null,
  onClose: null,
  className: '',
  children: ''
};

Modal.displayName = 'Modal';
Modal.style = StyledModal;

export default Modal;
