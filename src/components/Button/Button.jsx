import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import StyledButton, { StyledAnchor, contexts } from './Button.style';

const Button = ({
  type,
  context,
  shape,
  size,
  block,
  active,
  disabled,
  className,
  ...props
}) => {
  const classes = classnames(className, {
    btn: true,
    [`shape-${shape}`]: true,
    [`btn-${context}`]: true,
    [`btn-${size}`]: true,
    'btn-block': block,
    disabled
  });

  if (type === 'anchor') {
    return (
      <StyledAnchor
        className={classes}
        disabled={disabled}
        role="button"
        {...props}
      />
    );
  }

  return (
    <StyledButton
      className={classes}
      type={type}
      disabled={disabled}
      {...props}
    />
  );
};

Button.propTypes = {
  type: PropTypes.oneOf(['button', 'submit', 'reset', 'anchor']),
  context: PropTypes.oneOf(contexts),
  shape: PropTypes.oneOf(['square', 'round']),
  size: PropTypes.oneOf(['lg', 'md', 'sm', 'xs']),
  block: PropTypes.bool,
  active: PropTypes.bool,
  disabled: PropTypes.bool,
  className: PropTypes.string
};

Button.defaultProps = {
  type: 'button',
  context: 'default',
  shape: 'square',
  size: 'md',
  block: false,
  active: false,
  disabled: false,
  className: ''
};

Button.displayName = 'Button';
Button.style = StyledButton;
Button.contexts = contexts;

export default Button;
