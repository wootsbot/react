import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Transition from 'react-transition-group/Transition';

import Button from 'components/Button';
import Form from 'components/Form';

import StyledField from './Field.style';

const placeholder = 'A value is required';

export const statusValue = {
  ADD: 'add',
  MOD: 'mod',
  DEL: 'del'
};

const transitionStyles = {
  entered: {
    opacity: 1
  }
};

class Field extends React.Component {
  componentDidUpdate() {
    if (this.input) {
      if (this.props.isSelected) this.input.focus();
      else this.input.blur();

      if (this.props.value && this.input.type === 'text') {
        const position = this.props.value.length;
        this.input.setSelectionRange(position, position);
      }
    }
  }

  renderInput() {
    const { tag, value, modifyField } = this.props;

    const isValueTooLong = value && value.length > 15;

    return isValueTooLong ? (
      <Form.Textarea
        value={value || ''}
        placeholder={placeholder}
        innerRef={input => {
          this.input = input;
        }}
        onChange={event => modifyField(tag, event.target.value)}
        // we should probably add an onkeydown callback to close input on Enter
      />
    ) : (
      <Form.Input
        value={value || ''}
        placeholder={placeholder}
        innerRef={input => {
          this.input = input;
        }}
        onChange={event => modifyField(tag, event.target.value)}
        // we should probably add an onkeydown callback to close input on Enter
      />
    );
  }

  renderActions() {
    const { tag, status, isSelected, revertField, removeField } = this.props;

    return (
      <Transition
        in={isSelected}
        timeout={{
          enter: 0,
          exit: 300
        }}
        appear
        unmountOnExit
      >
        {state => (
          <div className="actions" style={transitionStyles[state]}>
            {status === 'mod' && (
              <Button
                shape="square"
                size="sm"
                onClick={() => revertField(tag)}
                className="action mod"
              >
                <i className="fas fa-undo" />
              </Button>
            )}
            {status !== 'del' && (
              <Button
                shape="square"
                size="sm"
                onClick={() => removeField(tag)}
                className="action del"
              >
                <i className="fas fa-times" />
              </Button>
            )}
          </div>
        )}
      </Transition>
    );
  }

  render() {
    const {
      tag,
      value,
      status,
      isSelected,
      isInactive,
      addField,
      selectField
    } = this.props;

    const isRemoved = status === statusValue.DEL;

    const classNames = classnames(status, {
      selected: isSelected,
      inactive: isInactive,
      removed: isRemoved,
      missing: !isRemoved && !value
    });

    return (
      <StyledField className={classNames}>
        <div className="main">
          <Button
            active={isSelected}
            shape="square"
            size="sm"
            className="tag"
            onClick={() => {
              if (isRemoved) addField(tag, value);
              else if (!isInactive) selectField(tag);
            }}
          >
            {tag}
          </Button>
          <div className="value">{value || placeholder}</div>
          {this.renderActions()}
        </div>
        {!isRemoved && this.renderInput()}
      </StyledField>
    );
  }
}

Field.propTypes = {
  tag: PropTypes.string.isRequired,
  status: PropTypes.string,
  value: PropTypes.string,
  isSelected: PropTypes.bool,
  isInactive: PropTypes.bool,
  addField: PropTypes.func,
  revertField: PropTypes.func,
  modifyField: PropTypes.func,
  removeField: PropTypes.func,
  selectField: PropTypes.func,
  className: PropTypes.string
};

Field.defaultProps = {
  status: null,
  value: null,
  isSelected: false,
  isInactive: false,
  addField: null,
  revertField: null,
  modifyField: null,
  removeField: null,
  selectField: null,
  className: ''
};

Field.displayName = 'Field';
Field.style = StyledField;

export default Field;
