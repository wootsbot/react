import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Loader from 'components/Loader';

import StyledItem, { StyledButton, StyledAnchor } from './styles/Item.style';

const ToolbarItem = ({
  // title,
  type,
  icon,
  size,
  shape,
  inactive,
  loading,
  // loaderTitle,
  inGroup,
  className,
  children,
  ...rest
}) => {
  const classes = classnames(className, {
    loading,
    [size]: size,
    [`shape-${shape}`]: true,
    btn: !inactive,
    'in-group': inGroup
  });

  let Element = StyledButton;

  if (inactive === false) {
    switch (type) {
      case 'anchor':
        Element = StyledAnchor;
        break;
      default:
        Element = StyledItem;
    }
  }

  if (loading) {
    const spinnerSizes = {
      xs: 12,
      sm: 14,
      md: 18,
      lg: 20
    };
    const strokeSizes = {
      xs: 2,
      sm: 2,
      md: 2,
      lg: 3
    };
    return (
      <Element size={size} className={classes} {...rest}>
        <Loader
          spinnerSize={spinnerSizes[size]}
          strokeSize={strokeSizes[size]}
          centered
        />
      </Element>
    );
  }

  return (
    <Element size={size} className={classes} {...rest}>
      {children && children}
      {!children && <i className={`fas fa-${icon}`} />}
    </Element>
  );
};

ToolbarItem.propTypes = {
  // title: PropTypes.string,
  type: PropTypes.oneOf(['button', 'anchor']),
  icon: PropTypes.string,
  size: PropTypes.oneOf(['xs', 'sm', 'md', 'lg']),
  shape: PropTypes.oneOf(['round', 'square']),
  inactive: PropTypes.bool,
  loading: PropTypes.bool,
  // loaderTitle: PropTypes.node,
  inGroup: PropTypes.bool,
  className: PropTypes.string,
  children: PropTypes.node
};

ToolbarItem.defaultProps = {
  // title: '',
  type: 'button',
  icon: '',
  size: 'md',
  shape: 'round',
  inactive: false,
  loading: false,
  // loaderTitle: '',
  inGroup: false,
  className: '',
  children: ''
};

ToolbarItem.displayName = 'Toolbar.Item';
ToolbarItem.style = StyledItem;

export default ToolbarItem;
