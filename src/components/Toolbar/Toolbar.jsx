import React from 'react';
import PropTypes from 'prop-types';
import Transition from 'react-transition-group/Transition';
import classnames from 'classnames';

import StyledToolbar from './styles/Toolbar.style';

class Toolbar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      opened: props.opened
    };
  }

  componentDidMount() {
    if (this.props.opened === true) {
      this._triggerCallback('onOpen');
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.opened !== nextProps.opened) {
      if (nextProps.opened === true) {
        this._triggerCallback('onOpen');
      } else {
        this._triggerCallback('onClose');
      }
    }

    this.setState({
      opened: nextProps.opened
    });
  }

  _triggerCallback(name) {
    if (this.props[name] !== null) {
      this.props[name]();
    }
  }

  render() {
    const {
      position,
      direction,
      size,
      shape,
      opacity,
      container,
      className,
      children,
      ...rest
    } = this.props;

    const asideClasses = classnames(className, {
      [`position-${position}`]: true,
      [`direction-${direction}`]: true,
      [`container-${container}`]: true,
      opened: this.state.opened
    });

    const childrenProps = {
      direction,
      position
    };

    if (size) {
      childrenProps.size = size;
    }

    if (shape) {
      childrenProps.shape = shape;
    }

    const transitionStyles = {
      entered: {
        opacity,
        transform: 'translate(0,0)'
      }
    };

    return (
      <Transition in={this.state.opened} timeout={100}>
        {state => (
          <StyledToolbar
            className={asideClasses}
            style={transitionStyles[state]}
            {...rest}
          >
            {React.Children.map(children, child =>
              React.cloneElement(child, childrenProps)
            )}
          </StyledToolbar>
        )}
      </Transition>
    );
  }
}

Toolbar.propTypes = {
  position: PropTypes.oneOf([
    'top-left',
    'top-center',
    'top-right',
    'center-right',
    'bottom-right',
    'bottom-center',
    'bottom-left',
    'center-left',
    'center-center'
  ]),
  direction: PropTypes.oneOf(['row', 'column']),
  size: PropTypes.oneOf(['', 'xs', 'sm', 'md', 'lg']),
  shape: PropTypes.oneOf(['', 'round', 'square']),
  container: PropTypes.oneOf(['parent', 'root']),
  opacity: PropTypes.number,
  opened: PropTypes.bool,
  onOpen: PropTypes.func,
  onClose: PropTypes.func,
  className: PropTypes.string,
  children: PropTypes.node.isRequired
};

Toolbar.defaultProps = {
  position: 'top-left',
  direction: 'column',
  size: '',
  shape: '',
  container: 'parent',
  opacity: 1,
  opened: false,
  onOpen: null,
  onClose: null,
  className: ''
};

Toolbar.displayName = 'Toolbar';
Toolbar.style = StyledToolbar;

export default Toolbar;
