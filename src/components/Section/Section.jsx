import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import StyledSection from './Section.style';

const Section = ({ appCanvas, className, ...props }) => {
  const classes = classnames(className, {
    'app-canvas': appCanvas
  });

  return <StyledSection className={classes} {...props} />;
};

Section.propTypes = {
  appCanvas: PropTypes.bool,
  dimmed: PropTypes.bool,
  className: PropTypes.string
};

Section.defaultProps = {
  appCanvas: false,
  dimmed: false,
  className: ''
};

Section.displayName = 'Section';
Section.style = StyledSection;

export default Section;
