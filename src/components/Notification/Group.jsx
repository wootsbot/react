import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import StyledGroup from './Group.style';

const NotificationGroup = ({
  position,
  direction,
  className,
  children,
  ...rest
}) => {
  const asideClasses = classnames(className, {
    [`position-${position}`]: true,
    [`direction-${direction}`]: true
  });

  return (
    <StyledGroup key="notification-group" className={asideClasses} {...rest}>
      {React.Children.map(children, child =>
        React.cloneElement(child, {
          className: 'notif-item',
          direction
        })
      )}
    </StyledGroup>
  );
};

NotificationGroup.propTypes = {
  position: PropTypes.oneOf([
    'top-left',
    'top-right',
    'bottom-right',
    'bottom-left',
    'top',
    'bottom'
  ]),
  direction: PropTypes.oneOf(['horizontal', 'vertical']),
  className: PropTypes.string,
  children: PropTypes.node.isRequired
};

NotificationGroup.defaultProps = {
  position: 'top-right',
  direction: 'horizontal',
  className: ''
};

NotificationGroup.displayName = 'Notification.Group';
NotificationGroup.style = StyledGroup;

export default NotificationGroup;
