import raw from '!!raw-loader!./basicSquare.svg';
import component from '!!svg-react-loader!./basicSquare.svg';

export default {
  iconAnchor: [25, 25],
  raw,
  component
};
