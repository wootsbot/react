import raw from '!!raw-loader!./basicUpTriangle.svg';
import component from '!!svg-react-loader!./basicUpTriangle.svg';

export default {
  iconAnchor: [25, 25],
  raw,
  component
};
