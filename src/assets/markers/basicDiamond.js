import raw from '!!raw-loader!./basicDiamond.svg';
import component from '!!svg-react-loader!./basicDiamond.svg';

export default {
  iconAnchor: [25, 25],
  raw,
  component
};
