import PropTypes from 'prop-types';
import _merge from 'lodash/merge';
import { colors } from 'constants/index';
import { buildDarkThemeConfig, themeFactory } from 'helpers/themes';
import { config as defaultThemeConfig } from './Default';

export const config = _merge(
  {},
  buildDarkThemeConfig(defaultThemeConfig, colors, 'darkGray'),
  {}
);

const DarkGrayTheme = themeFactory(config);

DarkGrayTheme.propTypes = {
  children: PropTypes.node.isRequired
};

DarkGrayTheme.displayName = 'DarkGrayTheme';

export default DarkGrayTheme;
