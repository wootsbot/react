import PropTypes from 'prop-types';
import _merge from 'lodash/merge';
import { colors } from 'constants/index';
import { buildDarkThemeConfig, themeFactory } from 'helpers/themes';
import { config as defaultThemeConfig } from './Default';

export const config = _merge(
  {},
  buildDarkThemeConfig(defaultThemeConfig, colors, 'brown'),
  {}
);

const BrownTheme = themeFactory(config);

BrownTheme.propTypes = {
  children: PropTypes.node.isRequired
};

BrownTheme.displayName = 'BrownTheme';

export default BrownTheme;
