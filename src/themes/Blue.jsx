import PropTypes from 'prop-types';
import _merge from 'lodash/merge';
import { colors } from 'constants/index';
import { buildDarkThemeConfig, themeFactory } from 'helpers/themes';
import { config as defaultThemeConfig } from './Default';

export const config = _merge(
  {},
  buildDarkThemeConfig(defaultThemeConfig, colors, 'blue'),
  {}
);

const BlueTheme = themeFactory(config);

BlueTheme.propTypes = {
  children: PropTypes.node.isRequired
};

BlueTheme.displayName = 'BlueTheme';

export default BlueTheme;
