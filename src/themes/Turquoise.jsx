import PropTypes from 'prop-types';
import { lighten } from 'polished';
import _merge from 'lodash/merge';
import { colors } from 'constants/index';
import { buildLightThemeConfig, themeFactory } from 'helpers/themes';
import { config as defaultThemeConfig } from './Default';

export const config = _merge(
  {},
  buildLightThemeConfig(defaultThemeConfig, colors, 'turquoise'),
  {
    sidebar: {
      nav: {
        color: colors.white,
        backgroundColor: colors.turquoise3,
        hoverColor: colors.white,
        hoverBackgroundColor: lighten(0.05, colors.turquoise3)
      }
    },
    form: {
      button: {
        info: {
          color: colors.white,
          backgroundColor: colors.turquoise1,
          borderColor: colors.turquoise1,
          hoverBackgroundColor: colors.turquoise3,
          hoverBorderColor: colors.turquoise3,
          focusBackgroundColor: colors.turquoise3,
          focusBorderColor: colors.turquoise3,
          activeBackgroundColor: colors.turquoise3,
          activeBorderColor: colors.turquoise3
        }
      }
    }
  }
);

const TurquoiseTheme = themeFactory(config);

TurquoiseTheme.propTypes = {
  children: PropTypes.node.isRequired
};

TurquoiseTheme.displayName = 'TurquoiseTheme';

export default TurquoiseTheme;
