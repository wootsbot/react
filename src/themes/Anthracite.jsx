import PropTypes from 'prop-types';
import _merge from 'lodash/merge';
import { colors } from 'constants/index';
import { buildDarkThemeConfig, themeFactory } from 'helpers/themes';
import { config as defaultThemeConfig } from './Default';

export const config = _merge(
  {},
  buildDarkThemeConfig(defaultThemeConfig, colors, 'anthracite'),
  {}
);

const AnthraciteTheme = themeFactory(config);

AnthraciteTheme.propTypes = {
  children: PropTypes.node.isRequired
};

AnthraciteTheme.displayName = 'AnthraciteTheme';

export default AnthraciteTheme;
