import React from 'react';
import { DefaultTheme, Button } from 'index';

import FakeApp from './FakeApp';

export class FakeSidebarCenter extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      opened: true
    };

    this.toggleSidebar = this.toggleSidebar.bind(this);
  }

  componentWillReceiveProps() {
    this.toggleSidebar(true);
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.opened !== prevState.opened && !this.state.opened) {
      const component = React.Children.toArray(this.props.children)[0];

      if (component.props.onClose) component.props.onClose();
    }
  }

  toggleSidebar(opened) {
    const o = opened !== undefined ? opened : !this.state.opened;

    this.setState({
      opened: o
    });
  }

  render() {
    const children = React.Children.map(this.props.children, component =>
      React.cloneElement(component, {
        opened: this.state.opened,
        onClose: () => this.toggleSidebar(false)
      })
    );

    return (
      <div>
        {children}
        <Button onClick={() => this.toggleSidebar()}>Toggle Sidebar</Button>
      </div>
    );
  }
}

const FakeSidebarApp = ({ children }) => (
  <DefaultTheme>
    <FakeApp fakeText>
      <FakeSidebarCenter>{children}</FakeSidebarCenter>
    </FakeApp>
  </DefaultTheme>
);

export default FakeSidebarApp;
