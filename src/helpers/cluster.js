// Compute a cluster indicator size
// 0 feature = minSize
// 50 features = maxSize
// In betwwen = minSize + ratio
export function buildClusterSize(minSize, maxSize, featureCount) {
  const sizeDiff = maxSize - minSize;
  const maxFeatureCount = 20;
  const count = featureCount > maxFeatureCount ? maxFeatureCount : featureCount;

  return minSize + (count * sizeDiff) / maxFeatureCount;
}
