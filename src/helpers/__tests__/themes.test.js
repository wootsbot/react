import { makeTransparent } from 'helpers/themes';

describe('makeTransparent', () => {
  it('should throw when not given an hexa string', () => {
    expect(() => makeTransparent({})).toThrow();
    expect(() => makeTransparent('{}')).toThrow();
  });

  it('should return a color with default opacity', () =>
    expect(makeTransparent('#000')).toEqual('rgba(0,0,0,0.5)'));

  it('should return a color with given opacity', () => {
    const opacity = 0.8;
    expect(makeTransparent('#000', opacity)).toEqual(`rgba(0,0,0,${opacity})`);
  });
});
